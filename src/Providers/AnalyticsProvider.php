<?php namespace Tekton\Wordpress\Analytics\Providers;

use Tekton\Support\ServiceProvider;

class AnalyticsProvider extends ServiceProvider {

    function register() {

    }

    function boot() {
        add_action( 'wp_footer', function() {
            $config = app('config');

            // Don't track Admin
            if ( ! current_user_can('manage_options') && $config->get('analytics.enabled', false)) {
                echo "<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', '".$config->get('analytics.trackingID')."', 'auto');
                  ga('send', 'pageview');

                </script>";
            }
        }, PHP_INT_MAX);

    }
}
